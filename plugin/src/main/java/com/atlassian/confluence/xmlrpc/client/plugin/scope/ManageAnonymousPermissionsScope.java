package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

public final class ManageAnonymousPermissionsScope extends ConfluenceScope
{
    public ManageAnonymousPermissionsScope()
    {
        super(ConfluencePermissions.MANAGE_ANONYMOUS_PERMISSIONS);
    }
}
