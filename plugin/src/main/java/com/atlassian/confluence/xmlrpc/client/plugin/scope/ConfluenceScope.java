package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceAdminClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceAttachmentClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceBlogClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceLabelClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceNotificationClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluencePageClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceSpaceClient;
import com.atlassian.confluence.xmlrpc.client.api.ConfluenceUserClient;
import com.atlassian.plugin.remotable.spi.permission.AbstractPermission;
import com.atlassian.plugin.remotable.spi.permission.scope.ApiResourceInfo;
import com.atlassian.plugin.remotable.spi.permission.scope.ApiScope;
import com.atlassian.plugin.remotable.spi.permission.scope.DownloadScopeHelper;
import com.atlassian.plugin.remotable.spi.permission.scope.JsonRpcApiScopeHelper;
import com.atlassian.plugin.remotable.spi.permission.scope.RestApiScopeHelper;
import com.atlassian.plugin.remotable.spi.permission.scope.XmlRpcApiScopeHelper;
import com.atlassian.plugin.remotable.spi.util.RequirePermission;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multimap;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import static com.atlassian.plugin.remotable.api.InstallationMode.LOCAL;
import static com.atlassian.plugin.remotable.api.InstallationMode.REMOTE;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;

abstract class ConfluenceScope extends AbstractPermission implements ApiScope
{
    private static final ImmutableMap<String, Collection<String>> serviceClassMethodsByPermission =
            ImmutableMap.copyOf(scanServiceClasses(
                    ConfluencePageClient.class,
                    ConfluenceAdminClient.class,
                    ConfluenceAttachmentClient.class,
                    ConfluenceLabelClient.class,
                    ConfluenceNotificationClient.class,
                    ConfluenceSpaceClient.class,
                    ConfluenceUserClient.class,
                    ConfluenceBlogClient.class
            ));


    private final XmlRpcApiScopeHelper v2XmlRpcApiScopeHelper;
    private final XmlRpcApiScopeHelper v1XmlRpcApiScopeHelper;
    private final JsonRpcApiScopeHelper v2JsonRpcScopeHelper;
    private final JsonRpcApiScopeHelper v1JsonRpcScopeHelper;
    private final RestApiScopeHelper restApiScopeHelper;
    private final Iterable<ApiResourceInfo> apiResourceInfo;

    private final DownloadScopeHelper downloadScopeHelper;

    protected ConfluenceScope(String key)
    {
        this(key, Collections.<RestApiScopeHelper.RestScope>emptyList());
    }

    protected ConfluenceScope(String key, Collection<RestApiScopeHelper.RestScope> resources)
    {
        this(key, resources, new DownloadScopeHelper());
    }

    protected ConfluenceScope(String key, Collection<RestApiScopeHelper.RestScope> resources, DownloadScopeHelper downloadScopeHelper)
    {
        super(key, ImmutableSet.of(LOCAL, REMOTE));

        final Collection<String> methods = serviceClassMethodsByPermission.get(key);

        this.v1JsonRpcScopeHelper = new JsonRpcApiScopeHelper("/rpc/json-rpc/confluenceservice-v1", methods);
        this.v2JsonRpcScopeHelper = new JsonRpcApiScopeHelper("/rpc/json-rpc/confluenceservice-v2", methods);
        this.v1XmlRpcApiScopeHelper = new XmlRpcApiScopeHelper("/rpc/xmlrpc", Collections2.transform(methods, xmlRpcTransform("confluence1")));
        this.v2XmlRpcApiScopeHelper = new XmlRpcApiScopeHelper("/rpc/xmlrpc", Collections2.transform(methods, xmlRpcTransform("confluence2")));
        this.restApiScopeHelper = new RestApiScopeHelper(checkNotNull(resources));
        this.downloadScopeHelper = checkNotNull(downloadScopeHelper);
        this.apiResourceInfo = concat(v1JsonRpcScopeHelper.getApiResourceInfos(),
                v2JsonRpcScopeHelper.getApiResourceInfos(),
                v1XmlRpcApiScopeHelper.getApiResourceInfos(),
                v2XmlRpcApiScopeHelper.getApiResourceInfos(),
                downloadScopeHelper.getApiResourceInfos());
    }

    @Override
    public final boolean allow(HttpServletRequest request, String user)
    {
        return v1XmlRpcApiScopeHelper.allow(request, user) ||
                v2XmlRpcApiScopeHelper.allow(request, user) ||
                v1JsonRpcScopeHelper.allow(request, user) ||
                v2JsonRpcScopeHelper.allow(request, user) ||
                restApiScopeHelper.allow(request, user) ||
                downloadScopeHelper.allow(request, user);
    }

    @Override
    public final Iterable<ApiResourceInfo> getApiResourceInfos()
    {
        return apiResourceInfo;
    }

    private Function<String, String> xmlRpcTransform(final String serviceName)
    {
        return new Function<String, String>()
        {
            @Override
            public String apply(String from)
            {
                return serviceName + "." + from;
            }
        };
    }

    private static Map<String, Collection<String>> scanServiceClasses(Class<?>... serviceInterfaces)
    {
        final Multimap<String, String> result = HashMultimap.create();
        for (Class serviceClass : serviceInterfaces)
        {
            for (Method method : serviceClass.getMethods())
            {
                final RequirePermission permission = method.getAnnotation(RequirePermission.class);
                if (permission != null)
                {
                    result.put(permission.value(), method.getName());
                }
            }
        }
        return result.asMap();
    }
}
