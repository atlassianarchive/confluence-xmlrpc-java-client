package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

public final class ModifyAttachmentsScope extends ConfluenceScope
{
    public ModifyAttachmentsScope()
    {
        super(ConfluencePermissions.MODIFY_ATTACHMENTS);
    }
}
