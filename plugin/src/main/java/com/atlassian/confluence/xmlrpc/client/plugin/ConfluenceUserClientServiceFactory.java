package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceUserClient;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.PluginAccessor;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceUserClient.class)
public class ConfluenceUserClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceUserClient>
{
    @Inject
    public ConfluenceUserClientServiceFactory(
                        @ComponentImport PermissionsReader permissionsReader,
                        @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceUserClient.class, permissionsReader, pluginAccessor);
    }
}