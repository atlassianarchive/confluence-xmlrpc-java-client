package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceNotificationClient;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.PluginAccessor;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceNotificationClient.class)
public class ConfluenceNotificationClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceNotificationClient>
{
    @Inject
    public ConfluenceNotificationClientServiceFactory(
                        @ComponentImport PermissionsReader permissionsReader,
                        @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceNotificationClient.class, permissionsReader, pluginAccessor);
    }
}

