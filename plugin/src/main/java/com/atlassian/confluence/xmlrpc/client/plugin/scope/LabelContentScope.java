package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

/**
 * API Scope for Confluence that grants Remotable Plugins the ability to add and remove labels from Confluence content.
 */
public final class LabelContentScope extends ConfluenceScope
{
    public LabelContentScope()
    {
        super(ConfluencePermissions.LABEL_CONTENT);
    }
}
