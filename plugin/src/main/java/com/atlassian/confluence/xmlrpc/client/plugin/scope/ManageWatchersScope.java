package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

/**
 * API Scope for Confluence that grants Remotable Plugins the ability to vuew and modify notification subscriptions ('watches')
 * for Confluence content.
 *
 * TODO: Consider splitting this up into separate read and write scopes
 */
public final class ManageWatchersScope extends ConfluenceScope
{
    public ManageWatchersScope()
    {
        super(ConfluencePermissions.MANAGE_WATCHERS);
    }
}
