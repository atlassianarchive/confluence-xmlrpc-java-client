package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.plugin.remotable.api.service.RequestContext;
import com.atlassian.plugin.remotable.api.service.http.HostHttpClient;
import com.atlassian.plugin.remotable.api.service.http.HostXmlRpcClient;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import com.atlassian.plugin.util.ChainingClassLoader;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceFactory;
import org.osgi.framework.ServiceRegistration;

import java.lang.reflect.Proxy;
import java.util.Set;

import static com.atlassian.plugin.remotable.spi.util.OsgiServiceProxy.wrapService;

/**
 * Creates confluence clients via proxies
 */
public abstract class AbstractConfluenceClientServiceFactory<T> implements ServiceFactory
{
    private final Class<T> clientClass;
    private final PermissionsReader permissionsReader;
    private final PluginAccessor pluginAccessor;


    protected AbstractConfluenceClientServiceFactory(Class<T> clientClass,PermissionsReader permissionsReader,
            PluginAccessor pluginAccessor)
    {
        this.clientClass = clientClass;
        this.permissionsReader = permissionsReader;
        this.pluginAccessor = pluginAccessor;
    }

    @Override
    public final Object getService(Bundle bundle, ServiceRegistration registration)
    {
        Plugin plugin = pluginAccessor.getPlugin(OsgiHeaderUtil.getPluginKey(bundle));
        if (plugin == null)
        {
            throw new IllegalArgumentException("Clients can only be used by plugins");
        }
        Set<String> permissions = permissionsReader.getPermissionsForPlugin(plugin);

        BundleContext bundleContext = bundle.getBundleContext();
        return clientClass.cast(Proxy.newProxyInstance(
                new ChainingClassLoader(getClass().getClassLoader(), clientClass.getClassLoader()),
                new Class[]{clientClass},
                new ClientInvocationHandler(
                        "confluence2",
                        wrapService(bundleContext, HostXmlRpcClient.class, getClass().getClassLoader()),
                        permissions,
                        plugin.getKey(),
                        wrapService(bundleContext, HostHttpClient.class, getClass().getClassLoader()),
                        wrapService(bundleContext, RequestContext.class, getClass().getClassLoader()))));
    }

    @Override
    public final void ungetService(Bundle bundle, ServiceRegistration registration, Object service)
    {
    }
}
