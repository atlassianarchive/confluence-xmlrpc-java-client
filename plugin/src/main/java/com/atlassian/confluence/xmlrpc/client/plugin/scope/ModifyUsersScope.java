package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

/**
 * API Scope for Confluence that grants Remotable Plugins the ability to change the details of user accounts in Confluence.
 */
public final class ModifyUsersScope extends ConfluenceScope
{
    public ModifyUsersScope()
    {
        super(ConfluencePermissions.MODIFY_USERS);
    }
}
