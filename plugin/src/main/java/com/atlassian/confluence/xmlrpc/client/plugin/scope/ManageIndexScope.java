package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

/**
 * API Scope for Confluence that grants Remotable Plugins the ability to add, edit and remove pages and blogs.
 */
public final class ManageIndexScope extends ConfluenceScope
{
    public ManageIndexScope()
    {
        super(ConfluencePermissions.MANAGE_INDEX);
    }
}
