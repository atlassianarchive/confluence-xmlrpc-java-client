package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceAdminClient;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.PluginAccessor;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceAdminClient.class)
public class ConfluenceAdminClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceAdminClient>
{
    @Inject
    public ConfluenceAdminClientServiceFactory(
            @ComponentImport PermissionsReader permissionsReader,
            @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceAdminClient.class, permissionsReader, pluginAccessor);
    }
}
