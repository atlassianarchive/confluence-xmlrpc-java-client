package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceBlogClient;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.PluginAccessor;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceBlogClient.class)
public class ConfluenceBlogClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceBlogClient>
{
    @Inject
    public ConfluenceBlogClientServiceFactory(
                        @ComponentImport PermissionsReader permissionsReader,
                        @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceBlogClient.class, permissionsReader, pluginAccessor);
    }
}

