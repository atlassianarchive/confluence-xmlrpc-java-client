package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceSpaceClient;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;
import com.atlassian.plugin.PluginAccessor;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceSpaceClient.class)
public class ConfluenceSpaceClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceSpaceClient>
{
    @Inject
    public ConfluenceSpaceClientServiceFactory(
                        @ComponentImport PermissionsReader permissionsReader,
                        @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceSpaceClient.class, permissionsReader, pluginAccessor);
    }
}

