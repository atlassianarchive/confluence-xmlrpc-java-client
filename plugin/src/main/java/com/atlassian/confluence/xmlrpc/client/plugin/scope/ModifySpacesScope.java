package com.atlassian.confluence.xmlrpc.client.plugin.scope;

import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;

public final class ModifySpacesScope extends ConfluenceScope
{
    public ModifySpacesScope()
    {
        super(ConfluencePermissions.MODIFY_SPACES);
    }
}
