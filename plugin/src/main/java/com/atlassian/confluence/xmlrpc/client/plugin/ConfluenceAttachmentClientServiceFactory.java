package com.atlassian.confluence.xmlrpc.client.plugin;

import com.atlassian.confluence.xmlrpc.client.api.ConfluenceAttachmentClient;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.remotable.api.annotation.ComponentImport;
import com.atlassian.plugin.remotable.api.annotation.PublicComponent;
import com.atlassian.plugin.remotable.spi.permission.PermissionsReader;

import javax.inject.Inject;

/**
 */
@PublicComponent(ConfluenceAttachmentClient.class)
public class ConfluenceAttachmentClientServiceFactory extends AbstractConfluenceClientServiceFactory<ConfluenceAttachmentClient>
{
    @Inject
    public ConfluenceAttachmentClientServiceFactory(
                        @ComponentImport PermissionsReader permissionsReader,
                        @ComponentImport PluginAccessor pluginAccessor)
    {
        super(ConfluenceAttachmentClient.class, permissionsReader, pluginAccessor);
    }
}
