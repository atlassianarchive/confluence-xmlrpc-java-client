<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <!-- api scopes -->
    <plugin-permission key="read_content" name="Read Content"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ReadContentScope"
                       application="confluence">
        <description>Read spaces, blogs, and pages and associated metadata</description>
    </plugin-permission>

    <plugin-permission key="modify_attachments" name="Modify Attachments"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ModifyAttachmentsScope"
                       application="confluence">
        <description>Add, remove, or move attachments for pages or blog posts</description>
    </plugin-permission>

    <plugin-permission key="read_users_and_groups" name="Read Users and Groups"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ConfluenceReadUsersAndGroupsScope"
                       application="confluence">
        <description>View users, users info, and groups</description>
    </plugin-permission>

    <plugin-permission key="modify_spaces" name="Modify Spaces"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ModifySpacesScope"
                       application="confluence">
        <description>Add, edit and remove spaces</description>
    </plugin-permission>
    <plugin-permission key="read_server_information" name="Read Server Information"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ReadServerInformationScope"
                       application="confluence">
        <description>View Confluence version number, base URL and build information</description>
    </plugin-permission>

    <plugin-permission key="modify_users" name="Modify Users"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ModifyUsersScope"
                       application="confluence">
        <description>Change existing users' information, including profile pictures.  Cannot change passwords.</description>
    </plugin-permission>

    <plugin-permission key="manage_watchers" name="Manage Watchers"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ManageWatchersScope"
                       application="confluence">
        <description>View space, page and blog post watchers. Add new watchers.</description>
    </plugin-permission>

    <plugin-permission key="label_content" name="Label Content"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.LabelContentScope"
                       application="confluence">
        <description>Add and remove labels from spaces, pages and blog posts</description>
    </plugin-permission>

    <plugin-permission key="render_content" name="Render Content"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.RenderContentScope"
                       application="confluence">
        <description>Convert content into viewable HTML</description>
    </plugin-permission>

    <plugin-permission key="modify_content" name="Modify Content"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ModifyContentScope"
                       application="confluence">
        <description>Add, edit and remove pages and blog posts</description>
    </plugin-permission>

    <plugin-permission key="manage_index" name="Manage Content Index"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ManageIndexScope"
                       application="confluence">
        <description>Manage the content index, including triggering a re-index</description>
    </plugin-permission>

    <plugin-permission key="manage_anonymous_permissions" name="Manage Anonymous Permissions"
                       class="com.atlassian.confluence.xmlrpc.client.plugin.scope.ManageAnonymousPermissionsScope"
                       application="confluence">
        <description>Manage anonymous permissions for the Confluence instance</description>
    </plugin-permission>
    
    <!-- These are only necessary until the Guice container is supported in-process -->
    <component key="adminClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceAdminClient"
               class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceAdminClientServiceFactory" 
               public="true" />
    
    <component key="attachmentClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceAttachmentClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceAttachmentClientServiceFactory"
                   public="true" />
    
    <component key="blogClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceBlogClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceBlogClientServiceFactory"
                   public="true" />
    
    <component key="labelClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceLabelClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceLabelClientServiceFactory"
                   public="true" />
    
    <component key="notificationClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceNotificationClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceNotificationClientServiceFactory"
                   public="true" />
    
    <component key="pageClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluencePageClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluencePageClientServiceFactory"
                   public="true" />
    
    <component key="spaceClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceSpaceClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceSpaceClientServiceFactory"
                   public="true" />
    
    <component key="userClient" interface="com.atlassian.confluence.xmlrpc.client.api.ConfluenceUserClient"
                   class="com.atlassian.confluence.xmlrpc.client.plugin.ConfluenceUserClientServiceFactory"
                   public="true" />

    <component-import key="permissionsReader" interface="com.atlassian.plugin.remotable.spi.permission.PermissionsReader" />
    <component-import key="pluginAccessor" interface="com.atlassian.plugin.PluginAccessor" />

</atlassian-plugin>
