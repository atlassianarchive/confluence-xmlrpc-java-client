package com.atlassian.confluence.xmlrpc.client.api;

import com.atlassian.confluence.xmlrpc.client.api.domain.BlogEntry;
import com.atlassian.confluence.xmlrpc.client.api.domain.BlogEntrySummary;
import com.atlassian.confluence.xmlrpc.client.api.domain.MutableBlogEntry;
import com.atlassian.plugin.remotable.api.confluence.ConfluencePermissions;
import com.atlassian.plugin.remotable.spi.util.RequirePermission;
import com.atlassian.util.concurrent.Promise;

/**
 */
public interface ConfluenceBlogClient
{
    @RequirePermission(ConfluencePermissions.READ_CONTENT)
    Promise<BlogEntry> getBlogEntryByDateAndTitle(String spaceKey, int year, int month, int dayOfMoth, String postTitle);

    @RequirePermission(ConfluencePermissions.READ_CONTENT)
    Promise<BlogEntry> getBlogEntry(long entryId);

    @RequirePermission(ConfluencePermissions.READ_CONTENT)
    Promise<BlogEntrySummary> getBlogEntries(String spaceKey);

    @RequirePermission(ConfluencePermissions.MODIFY_CONTENT)
    Promise<BlogEntry> storeBlogEntry(MutableBlogEntry blogEntry);
}
