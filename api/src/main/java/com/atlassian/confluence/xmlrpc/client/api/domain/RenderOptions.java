package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface RenderOptions
{
    void setStyle(RenderStyle style);
}
