package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface MutableBlogEntry
{
    void setId(long id);

    void setSpaceKey(String spaceKey);

    void setTitle(String title);

    void setContent(String content);

    long getId();

    String getSpaceKey();

    String getTitle();

    String getContent();
}
