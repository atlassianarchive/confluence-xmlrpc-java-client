package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface MutableLabel
{
    void setId(long id);

    long getId();
}
