package com.atlassian.confluence.xmlrpc.client.api.domain;

import com.atlassian.plugin.remotable.spi.util.RemoteName;

/**
 */
public enum ContentPermissionType
{
    @RemoteName("View")
    VIEW,

    @RemoteName("Edit")
    EDIT
}
