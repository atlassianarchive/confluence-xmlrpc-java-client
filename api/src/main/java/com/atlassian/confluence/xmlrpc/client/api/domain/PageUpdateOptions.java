package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface PageUpdateOptions
{
    void setVersionComment(String versionComment);

    void setMinorEdit(boolean minorEdit);
}
