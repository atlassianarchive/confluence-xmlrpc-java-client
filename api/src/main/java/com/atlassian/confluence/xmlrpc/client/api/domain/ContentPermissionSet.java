package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface ContentPermissionSet
{
    ContentPermissionType getType();

    Iterable<ContentPermission> getContentPermissions();
}
