package com.atlassian.confluence.xmlrpc.client.api.domain;

import java.net.URI;

/**
 */
public interface SpaceSummary
{
    String getName();

    String getKey();

    URI getUrl();

    SpaceType getType();
}
