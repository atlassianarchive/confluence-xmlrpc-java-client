package com.atlassian.confluence.xmlrpc.client.api.domain;

import com.atlassian.plugin.remotable.spi.util.RemoteName;

/**
 */
public enum SpaceType
{
    @RemoteName("global")
    GLOBAL,

    @RemoteName("personal")
    PERSONAL
}
