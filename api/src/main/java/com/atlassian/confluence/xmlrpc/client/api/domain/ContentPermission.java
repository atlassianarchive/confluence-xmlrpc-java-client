package com.atlassian.confluence.xmlrpc.client.api.domain;

/**
 */
public interface ContentPermission
{
    ContentPermissionType getType();

    String getUserName();

    String getGroupName();
}
