package com.atlassian.confluence.xmlrpc.client.api.domain;

import com.atlassian.plugin.remotable.spi.util.RemoteName;

/**
 */
public interface ContentSummaries
{
    int getTotalAvailable();

    int getOffset();

    @RemoteName("content")
    Iterable<ContentSummary> getContentSummaries();
}
