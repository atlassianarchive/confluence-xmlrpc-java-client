package com.atlassian.confluence.xmlrpc.client.api.domain;

import java.net.URI;

/**
 */
public interface SearchResult
{
    String getTitle();

    URI getUrl();

    String getExcerpt();

    ContentType getType();

    Long getId();
}
