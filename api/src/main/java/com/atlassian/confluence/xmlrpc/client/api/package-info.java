/**
 * This package is contains interfaces for a java client to the Confluence XML-RPC interface
 */
package com.atlassian.confluence.xmlrpc.client.api;